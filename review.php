<?php

class Review {
    private $menuName;
    private $body;
    private $user;

    public function __construct($menuName, $user , $body) {
        $this->menuName = $menuName;
        $this->body = $body;
        $this->user = $user;
    }

    public function getMenuName() {
        return $this->menuName;
    }
    public function getBody() {
        return $this->body;
    }

    public function getUser() {
        return $this->user;
    }

    // public function getUser($users) {
    //     foreach($users as $user) {
    //         if ($user->getUserId() == $this->userId) {
    //             return $user;
    //         }
    //     }
    // }
}

?>