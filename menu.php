<?php

class Menu {
    protected $menuName;
    protected $price;
    protected $imageURL;

    private $orderCount;
    protected static $orders;

    public function __construct($menuName, $price, $imageURL) {
        $this->menuName = $menuName;
        $this->price = $price;
        $this->imageURL = $imageURL;
        self::$orders++;
    }

    public function ab() {
        return "HElllooo";
    }

    public function getImageURL() {
        return $this->imageURL;
    }

    public function getMenuName() {
        return $this->menuName;
    }

    public function getOrders() {
        return self::$orders;
    }

    public function getOrderCount() {
        return $this->orderCount;
    }
    public function setOrderCount($orderCount) {
        $this->orderCount = $orderCount;
    }
    public function getTaxIncludedPrice() {
        return round($this->price * 1.0725, 2);
    }
    
    public function getTotalPrice() {
        return $this->getTaxIncludedPrice() * $this->orderCount;
    }
    
    public static function findByName($menus, $menuName) {
        foreach ($menus as $menu) {
            if ($menu->getMenuName() == $menuName) {
                return $menu;
            }
        }
    } 
    public function getReviews($reviews) {
        $reviewsForMenu = array();
        foreach ($reviews as $review) {
            if ($review->getMenuName() == $this->menuName) {
                $reviewsForMenu[] = $review;
            }
        }
        return $reviewsForMenu;
    }
}

?>