<?php 

class User {
    private $username;
    private $gender;
    private $userId;
    private static $count = 0;

    public function __construct($username, $gender) {
        $this->username = $username;
        $this->gender = $gender;
        self::$count++;
        $this->userId = self::$count;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getUserName() {
        return $this->username;
    }
    public function getGender() {
        return $this->gender;
    }
}

?>